let firstData = prompt(
  "Enter array",
  `"hello", 64, "world", 23, 0, undefined, "23", null, false, true, 56, "name", Value-W`
);

let type = prompt("Enter type", `number`);

let inputArray = firstData.split(", ");

function replaceValue(array) {
  return array.map((item) => {
    if (isNaN(item) || (item.startsWith('"') && item.endsWith('"'))) {
      return item;
    } else if (item === "undefined") {
      return item === undefined;
    } else if (item === "true" || item === "false") {
      return Boolean(item);
    } else {
      return Number(item);
    }
  });
}

let arr = replaceValue(inputArray);

console.log(`Initial array: ${arr}`);

function filterBy(arr, type) {
  return arr.filter((item) => typeof item !== type);
}

console.log(filterBy(arr, type));

// const allTypes = ['string', 'number', 'boolean', 'object', 'undefined', 'symbol', 'bigint' ];
// allTypes.forEach(type => console.log(filterBy(arr, type)));
// console.log(filterBy(arr, "number"));
// console.log(filterBy(arr, "string"));
// console.log(filterBy(arr, "boolean"));
// console.log(filterBy(arr, "undefined"));
